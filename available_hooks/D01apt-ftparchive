#!/bin/bash

# Optionally use the changelog of a package to determine the suite to use if
# none set.
if [ -z "${DIST}" ] && [ -r "debian/changelog" ]; then
    DIST=$(dpkg-parsechangelog | awk '/^Distribution: / {print $2}')
    # Use the unstable suite for certain suite values.
    if $(echo "experimental UNRELEASED" | grep -q ${DIST}); then
        DIST="${UNSTABLE_CODENAME}"
    fi
    # Use the stable suite for stable-backports.
    if $(echo "${STABLE_BACKPORTS_SUITE}" | grep -q ${DIST}); then
        DIST="${STABLE}"
    fi
fi

# Optionally set a default distribution if none is used. Note that you can set
# your own default (i.e. ${DIST:="unstable"}).
: ${DIST:="$(lsb_release --short --codename)"}

# Optionally change Debian release states in $DIST to their names.
case "${DIST}" in
    unstable)
        DIST="${UNSTABLE_CODENAME}"
        ;;
    testing)
        DIST="${TESTING_CODENAME}"
        ;;
    stable)
        DIST="${STABLE_CODENAME}"
        ;;
esac

# Optionally set the architecture to the host architecture if none set. Note
# that you can set your own default (i.e. ${ARCH:="i386"}).
: ${ARCH:="$(dpkg --print-architecture)"}

NAME="${DIST}"
if [ -n "${ARCH}" ]; then
    NAME="${NAME}-${ARCH}"
    DEBOOTSTRAPOPTS=("--arch" "${ARCH}" "${DEBOOTSTRAPOPTS[@]}")
fi

# May be necessary if base image does not have apt-utils already.
apt-get install apt-utils

# We want this to be up to date for _every_ run for magic to happen!

( cd /var/cache/pbuilder/${NAME}/result/ ;
  apt-ftparchive packages . > /var/cache/pbuilder/${NAME}/result/Packages
)

cat<<EOF >/var/cache/pbuilder/${NAME}/result/Release
Archive: stable
Component: main
Origin: pbuilder
Label: pbuilder
Architecture: i386 amd64
EOF

cat<<EOF >/etc/apt/sources.list
# deb file:///var/cache/pbuilder/${NAME}/result ./
deb http://ftp.de.debian.org/debian/ squeeze main contrib non-free
deb-src http://ftp.de.debian.org/debian/ squeeze main contrib non-free
deb http://backports.debian.org/debian-backports squeeze-backports main
deb-src http://backports.debian.org/debian-backports squeeze-backports main
deb http://ftp.de.debian.org/debian/ testing main contrib non-free
deb-src http://ftp.de.debian.org/debian/ testing main contrib non-free
EOF

cat<<EOF >/etc/apt/preferences
Package: *
Pin: release o=pbuilder
Pin-Priority: 701

Package: *
Pin: release a=squeeze
Pin-Priority: 600

Package: *
Pin: release a=testing
Pin-Priority: 900

Package: *
Pin: release a=squeeze-backports
Pin-Priority: 100
EOF

apt-get update


