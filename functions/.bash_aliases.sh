#!/bin/bash
alias squeeze_cow_build='sudo env DIST=squeeze ARCH=amd64 cowbuilder --build --mountproc --mountdev --mountdevpts'
alias squeeze_cow_build_save='sudo env DIST=squeeze ARCH=amd64 cowbuilder --build --mountproc --mountdev --mountdevpts --save-after-login'
alias squeeze_cow_login='sudo env DIST=squeeze ARCH=amd64 cowbuilder --login --mountproc --mountdev --mountdevpts'
alias squeeze_cow_login_save='sudo env DIST=squeeze ARCH=amd64 cowbuilder --login --mountproc --mountdev --mountdevpts --save-after-login'

alias sid_cow_build='sudo env DIST=sid ARCH=amd64 cowbuilder --build --mountproc --mountdev --mountdevpts'
alias sid_cow_build_save='sudo env DIST=sid ARCH=amd64 cowbuilder --build --mountproc --mountdev --mountdevpts --save-after-login'
alias sid_cow_login='sudo env DIST=sid ARCH=amd64 cowbuilder --login --mountproc --mountdev --mountdevpts'
alias sid_cow_login_save='sudo env DIST=sid ARCH=amd64 cowbuilder --login --mountproc --mountdev --mountdevpts --save-after-login'

alias wheezy_cow_build='sudo env DIST=wheezy ARCH=amd64 cowbuilder --build --mountproc --mountdev --mountdevpts'
alias wheezy_cow_build_save='sudo env DIST=wheezy ARCH=amd64 cowbuilder --build --mountproc --mountdev --mountdevpts --save-after-login'
alias wheezy_cow_login='sudo env DIST=wheezy ARCH=amd64 cowbuilder --login --mountproc --mountdev --mountdevpts'
alias wheezy_cow_login_save='sudo env DIST=wheezy ARCH=amd64 cowbuilder --login --mountproc --mountdev --mountdevpts --save-after-login'